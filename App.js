import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  StatusBar
} from 'react-native';
import { Provider } from 'react-redux';

import NavigationService from './src/helpers/NavigationService';
import { MainNavigator } from './src/helpers/PageStructure';
import { BottomMenu } from './src/components/Menus'
import store from './src/store';
import * as colors from './src/helpers/ColorPalette';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store} >
        <View style={styles.container}>
          <StatusBar hidden />
          <MainNavigator
            ref={navigatorRef => { NavigationService.setTopLevelNavigator(navigatorRef) }} 
          />
          <BottomMenu />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});
