import {
  SHOW_BOTTOM_MENU_BIG_BUTTON,
  SHOW_BOTTOM_MENU
} from '../helpers/ActionNames.js';

export const showBottomMenuBigButtonAction = showBigButton => {
  return {
    type: SHOW_BOTTOM_MENU_BIG_BUTTON,
    payload: showBigButton
  };
};

export const showBottomMenuAction = showMenu => {
  return {
    type: SHOW_BOTTOM_MENU,
    payload: showMenu
  };
};
