import React, { Component } from 'react';
import { View, Text, Image, Animated, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';

import { CenteredArea } from '../CenteredAreas';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';
import styles from './styles';

class HomeOwnerCard extends Component {
  state = {
    starCount: 4.1,
    animateItem: new Animated.Value(0)
  };

  componentWillMount() {
    const { index } = this.props;
    const delay = index * 50;

    // Animated.sequence([
    //   Animated.delay(delay),
    //   Animated.spring(this.state.animateItem, {
    //     toValue: 1,
    //     velocity: 1
    //   })
    // ]).start();

    Animated.timing(this.state.animateItem, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true
    }).start();

    this.flipAnimatedValue = new Animated.Value(0);
    this.value = 0;
    this.flipAnimatedValue.addListener(({ value }) => {
      this.value = value;
    });

    this.frontInterpolate = this.flipAnimatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['0deg', '180deg']
    });

    this.backInterpolate = this.flipAnimatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['180deg', '360deg']
    });
  }

  flipCard = () => {
    if (this.value > 90) {
      Animated.spring(this.flipAnimatedValue, {
        toValue: 0,
        friction: 8,
        tension: 10,
        useNativeDriver: true
      }).start();
    } else {
      Animated.spring(this.flipAnimatedValue, {
        toValue: 180,
        friction: 8,
        tension: 10,
        useNativeDriver: true
      }).start();
    }
  };

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
  render() {
    const { animateItem } = this.state;
    const { user, index, handleOpenImage } = this.props;

    const frontAnimatedStyle = {
      transform: [{ rotateY: this.frontInterpolate }]
    };

    const backAnimatedStyle = {
      transform: [{ rotateY: this.backInterpolate }]
    };

    return (
      <CenteredArea>
        <Animated.View
          style={{
            opacity: animateItem,
            transform: [
              {
                translateX: animateItem.interpolate({
                  inputRange: [0, 1],
                  outputRange:
                    index % 2 === 0 ? [defaults.WIDTH, 1] : [-defaults.WIDTH, 1]
                })
              }
            ]
          }}
        >
          <TouchableOpacity onPress={() => this.flipCard()}>
            <Animated.View
              style={[styles.homeOwnerCardContainerStyle, frontAnimatedStyle]}
            >
              <Image
                source={{
                  uri: user.image
                }}
                style={[styles.homeOwnerCardImageStyle]}
                resizeMode='contain'
              />

              <View style={styles.homeOwnerInfoContainerStyle}>
                <Text style={{ fontSize: 18 }}>{user.name}</Text>
                <Text style={{ fontSize: 14 }}>{user.gender}</Text>
              </View>

              <View style={styles.homeOwnerStarRatingContainerStyle}>
                <Text>112 votes </Text>

                <StarRating
                  disabled={false}
                  maxStars={5}
                  fullStarColor={colors.orange}
                  halfStarColor={colors.orange}
                  emptyStarColor={colors.orange}
                  rating={this.state.starCount}
                  selectedStar={rating => this.onStarRatingPress(rating)}
                  animation={null}
                  disabled
                  starSize={12}
                />
              </View>
            </Animated.View>
          </TouchableOpacity>

          <View style={{ position: 'absolute', top: 0 }}>
            <TouchableOpacity onPress={() => this.flipCard()}>
              <Animated.View
                style={[
                  styles.homeOwnerCardContainerStyle,
                  { backgroundColor: colors.orange },
                  backAnimatedStyle
                ]}
              >
                <Image
                  source={{
                    uri: user.image
                  }}
                  style={[styles.homeOwnerCardImageStyle]}
                  resizeMode='contain'
                />

                <View style={styles.homeOwnerInfoContainerStyle}>
                  <Text style={{ fontSize: 18 }}>{user.name}</Text>
                  <Text style={{ fontSize: 14 }}>{user.gender}</Text>
                </View>

                <View style={styles.homeOwnerStarRatingContainerStyle}>
                  <Text>112 votes </Text>

                  <StarRating
                    disabled={false}
                    maxStars={5}
                    fullStarColor={colors.orange}
                    halfStarColor={colors.orange}
                    emptyStarColor={colors.orange}
                    rating={this.state.starCount}
                    selectedStar={rating => this.onStarRatingPress(rating)}
                    animation={null}
                    disabled
                    starSize={12}
                  />
                </View>
              </Animated.View>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </CenteredArea>
    );
  }
}

export default HomeOwnerCard;
