import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const AREA_WIDTH = WINDOW_WIDTH - 32;
const INFO_BOX_SIZE = 96;

export default StyleSheet.create({
  homeOwnerCardContainerStyle: {
    width: AREA_WIDTH,
    borderWidth: 1,
    borderRadius: 12,
    backgroundColor: colors.white,
    borderColor: colors.orange,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginVertical: 8,
    backfaceVisibility: 'hidden',
    borderWidth: 2,
    borderColor: colors.blue
  },
  homeOwnerCardImageStyle: {
    width: INFO_BOX_SIZE,
    height: INFO_BOX_SIZE,
    margin: 12,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: colors.orange
  },
  homeOwnerInfoContainerStyle: {
    height: INFO_BOX_SIZE,
    alignItems: 'flex-end',
    margin: 12,
    marginLeft: 32
  },
  homeOwnerStarRatingContainerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    right: 4,
    bottom: 4
  }
});
