import { StyleSheet, Dimensions } from 'react-native';

const WINDOW_WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  centeredAreaContainerStyle: {
    width: WINDOW_WIDTH,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centeredTextContainerStyle: {
    width: WINDOW_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16
  }
});
