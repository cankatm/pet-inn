import React, { Component } from 'react';
import { View } from 'react-native';

import styles from './styles';

class CenteredArea extends Component {
  static defaultProps = {
    ...Component.defaultProps,
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: 'transparent',
    flexDirection: 'column'
  };

  render() {
    const {
      children,
      marginTop,
      marginBottom,
      backgroundColor,
      flexDirection
    } = this.props;
    return (
      <View
        style={[
          styles.centeredAreaContainerStyle,
          { marginTop, marginBottom, backgroundColor, flexDirection }
        ]}
      >
        {children}
      </View>
    );
  }
}

export default CenteredArea;
