import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const WINDOW_WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  backHeaderContainerStyle: {
    position: 'absolute',
    right: 0,
    left: 0,
    width: defaults.WIDTH,
    height: defaults.headerHeight,
    backgroundColor: colors.orange
  },
  centeredTextContainerStyle: {
    width: WINDOW_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16
  }
});
