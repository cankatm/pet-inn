import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Animated } from 'react-native';
import {
  withNavigation,
  StackActions,
  NavigationActions
} from 'react-navigation';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { showBottomMenuBigButtonAction } from '../../actions/BottomMenuActions';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';
import styles from './styles';

class BackHeader extends Component {
  handleResetToMainMenu = () => {
    const { navigation, showBottomMenuBigButtonAction } = this.props;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'SearchPage' })]
    });
    navigation.dispatch(resetAction);
    showBottomMenuBigButtonAction(true);
  };

  render() {
    const { translateY, scrollAnimation } = this.props;

    let translateTextY = scrollAnimation.interpolate({
      inputRange: [0, defaults.headerHeightAnimationValue / 2],
      outputRange: [0, defaults.headerHeightAnimationValue / 2],
      extrapolate: 'clamp'
    });

    let opacityText = scrollAnimation.interpolate({
      inputRange: [0, defaults.headerHeightAnimationValue / 2],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });

    return (
      <Animated.View
        style={[
          styles.backHeaderContainerStyle,
          { transform: [{ translateY }] }
        ]}
      >
        <Animated.View
          style={[
            {
              position: 'absolute',
              top: defaults.headerHeight / 2 - 20,
              left: 0
            },
            { transform: [{ translateY: translateTextY }] }
          ]}
        >
          <TouchableOpacity onPress={() => this.handleResetToMainMenu()}>
            <View
              style={{
                width: 40,
                height: 40,
                backgroundColor: colors.transparent
              }}
            >
              <View style={{ marginLeft: 10 }}>
                <Icon name='ios-arrow-back' size={30} color={colors.white} />
              </View>
            </View>
          </TouchableOpacity>
        </Animated.View>

        <Animated.View
          style={[
            {
              position: 'absolute',
              top: defaults.headerHeight / 2 - 10,
              left: (defaults.WIDTH - 160) / 2
            },
            {
              opacity: opacityText,
              transform: [{ translateY: translateTextY }]
            }
          ]}
        >
          <View style={{ width: 160, alignItems: 'center' }}>
            <Text style={{ color: colors.white, fontSize: 18 }}>
              Search Results
            </Text>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }
}

export default withNavigation(
  connect(
    null,
    { showBottomMenuBigButtonAction }
  )(BackHeader)
);
