import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import LottieView from 'lottie-react-native';
import { connect } from 'react-redux';

import BottomMenuItem from './BottomMenuItem';
import BottomMenuCloseItem from './BottomMenuCloseItem';
import { showBottomMenuAction } from '../../actions';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

import animation1 from '../../../assets/animations/menuButton1.json';
import animation2 from '../../../assets/animations/menuButton2.json';
import animation3 from '../../../assets/animations/hamburger.json';

class BottomMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuOpenedAtLeastOnce: false,
      animation: animation3,
      animationPlayed: false,
      isDisabled: false
    };
  }

  _playAnimation = () => {
    if (!this.state.animationPlayed) {
      this.setState({ isDisabled: true });
      setTimeout(() => this.setState({ isDisabled: false }), 250);
      this.animation.play(0.0, 60.0);
      this.setState({ animationPlayed: true });
    } else {
      this.setState({ isDisabled: true });
      setTimeout(() => this.setState({ isDisabled: false }), 250);
      this.animation.play(60.0, 0.0);
      this.setState({ animationPlayed: false });
    }
  };

  renderBottomMenuItems = () => {
    if (this.props.showBottomMenu) {
      return (
        <View>
          <BottomMenuItem
            index={0}
            bottom={40}
            left={defaults.WIDTH / 2 - defaults.bottomMenuSize - 24}
            iconName='ios-search'
            goTo='SearchPage'
            isDisabled={this.state.isDisabled}
            _playAnimation={this._playAnimation}
          />
          <BottomMenuItem
            index={1}
            bottom={100}
            left={defaults.WIDTH / 2 - defaults.bottomMenuSize + 20}
            iconName='ios-star'
            goTo='FavoritesPage'
            isDisabled={this.state.isDisabled}
            _playAnimation={this._playAnimation}
          />
          <BottomMenuItem
            index={2}
            bottom={100}
            right={defaults.WIDTH / 2 - defaults.bottomMenuSize + 20}
            iconName='ios-send'
            goTo='MessagesPage'
            isDisabled={this.state.isDisabled}
            _playAnimation={this._playAnimation}
          />
          <BottomMenuItem
            index={3}
            bottom={40}
            right={defaults.WIDTH / 2 - defaults.bottomMenuSize - 24}
            iconName='ios-person'
            goTo='MyProfilePage'
            isDisabled={this.state.isDisabled}
            _playAnimation={this._playAnimation}
            notification={!!false} // TODO: buradaki true koşulu noticitaion varsa olacak
          />
        </View>
      );
    } else if (!this.props.showBottomMenu && this.state.menuOpenedAtLeastOnce) {
      //it has no purpose but closing animation
      return (
        <View>
          <BottomMenuCloseItem
            index={3}
            bottom={40}
            left={defaults.WIDTH / 2 - defaults.bottomMenuSize - 24}
            iconName='ios-search'
          />
          <BottomMenuCloseItem
            index={2}
            bottom={100}
            left={defaults.WIDTH / 2 - defaults.bottomMenuSize + 20}
            iconName='ios-star'
          />
          <BottomMenuCloseItem
            index={1}
            bottom={100}
            right={defaults.WIDTH / 2 - defaults.bottomMenuSize + 20}
            iconName='ios-send'
          />
          <BottomMenuCloseItem
            index={0}
            bottom={40}
            right={defaults.WIDTH / 2 - defaults.bottomMenuSize - 24}
            iconName='ios-person'
          />
        </View>
      );
    }
  };

  render() {
    const {
      showBottomMenuBigButton,
      showBottomMenuAction,
      showBottomMenu
    } = this.props;

    return (
      showBottomMenuBigButton && (
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            width: defaults.WIDTH
          }}
        >
          <View style={styles.bottomMenuContainerStyle}>
            <TouchableOpacity
              disabled={this.state.isDisabled}
              activeOpacity={1.0}
              onPress={() => {
                showBottomMenuAction(!this.props.showBottomMenu),
                  this.setState({ menuOpenedAtLeastOnce: true }),
                  this._playAnimation();
              }}
            >
              <View style={styles.bottomMenuInnerContainerStyle}>
                <View style={{ marginLeft: 16, marginTop: 15 }}>
                  {this.state.animation && (
                    <LottieView
                      speed={3.5}
                      loop={false}
                      ref={animation => {
                        this.animation = animation;
                      }}
                      style={{
                        width: 50,
                        height: 50
                      }}
                      source={this.state.animation}
                    />
                  )}
                </View>
                {false && !showBottomMenu && (
                  <View style={styles.bottomMenuNotificationContainerStyle} />
                )}
                {/* TODO: true yerine notification */}
              </View>
            </TouchableOpacity>
          </View>
          {this.renderBottomMenuItems()}
        </View>
      )
    );
  }
}

const mapStateToProps = state => {
  const { showBottomMenuBigButton, showBottomMenu } = state.bottomMenuReducer;

  return { showBottomMenuBigButton, showBottomMenu };
};

export default connect(
  mapStateToProps,
  { showBottomMenuAction }
)(BottomMenu);
