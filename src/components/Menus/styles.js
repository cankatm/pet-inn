import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
  bottomMenuContainerStyle: {
    position: 'absolute',
    bottom: 16,
    left: defaults.WIDTH / 2 - defaults.bottomMenuSize / 2
  },
  bottomMenuNotificationContainerStyle: {
    width: 10,
    height: 10,
    borderRadius: 4,
    backgroundColor: colors.blue,
    position: 'absolute',
    right: 20,
    bottom: 26,
    zIndex: 999
  },
  bottomMenuInnerContainerStyle: {
    width: defaults.bottomMenuSize,
    height: defaults.bottomMenuSize,
    borderRadius: defaults.bottomMenuSize / 2,
    backgroundColor: colors.orange
  },
  bottomMenuItemContainerStyle: {
    position: 'absolute'
  },
  bottomMenuItemInnerContainerStyle: {
    width: defaults.bottomMenuSmallIconsSize,
    height: defaults.bottomMenuSmallIconsSize,
    borderRadius: defaults.bottomMenuSmallIconsSize / 2,
    backgroundColor: colors.orange,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottomMenuItemNotificationContainerStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: colors.blue,
    position: 'absolute',
    right: 10,
    bottom: 12,
    zIndex: 999
  }
});
