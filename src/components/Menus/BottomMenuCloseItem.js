import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

class BottomMenuCloseItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animateItem: new Animated.Value(1)
    };
  }

  componentWillMount() {
    const { index } = this.props;
    const delay = index * 50;

    Animated.timing(this.state.animateItem, {
      toValue: 0,
      duration: 100,
      delay
    }).start();
  }

  render() {
    const { bottom, top, left, right, iconName } = this.props;

    return (
      <Animated.View
        style={[
          styles.bottomMenuItemContainerStyle,
          {
            bottom,
            left,
            top,
            right,
            opacity: this.state.animateItem,
            transform: [{ scale: this.state.animateItem }]
          }
        ]}
      >
        <View style={styles.bottomMenuItemInnerContainerStyle}>
          <Icon name={iconName} size={24} color={colors.white} />
        </View>
      </Animated.View>
    );
  }
}

export default BottomMenuCloseItem;
