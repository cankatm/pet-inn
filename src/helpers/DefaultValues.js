import { Dimensions } from 'react-native';

export const WIDTH = Dimensions.get('window').width;
export const HEIGHT = Dimensions.get('window').height;

export const bottomMenuSize = 80;
export const bottomMenuSmallIconsSize = 50;

export const homePageButtonSize = 128;
export const homePageIconSize = 32;

export const headerHeight = 100;
export const headerHeightAnimationValue = 60;
