export const white = '#fff';
export const black = '#000';
export const pink = '#d12e96';
export const red = 'red';

export const lightGrey = '#ccc';
export const orange = '#FC7F09';
export const blue = '#0380F6';

export const transparent = 'transparent';
