//Dummy data of users for testing

export const dummyUsers = [
  {
    id: '1',
    name: 'Mert Cankat',
    gender: '253₺',
    image: 'https://picsum.photos/96/96',
    email: 'test@pvh.com',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '2',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '3',
    name: 'Mert Cankat',
    gender: '253₺',
    image: 'https://picsum.photos/96/96',
    email: 'test@pvh.com',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '4',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '5',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '6',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '7',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '8',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '9',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '10',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  },
  {
    id: '11',
    name: 'Mert Cankat',
    gender: '253₺',
    email: 'test@pvh.com',
    image: 'https://picsum.photos/96/96',
    phone: '+31612345678',
    address: [
      {
        number: '7',
        street: 'Danzigerkade',
        city: 'Amsterdam',
        zipcode: '1234 AB'
      },
      {
        number: '44',
        street: 'Oran',
        city: 'Ankara',
        zipcode: '06450'
      }
    ]
  }
];
