import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from './ColorPalette';
import * as defaults from './DefaultValues';

import {
  SearchPage,
  SearchResultsPage,
  FavoritesPage,
  MessagesPage,
  MyProfilePage,
  HomeOwnerPage
} from '../pages';

const FadeTransition = (index, position) => {
  const sceneRange = [index - 1, index];
  const outputOpacity = [0, 1];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputOpacity
  });

  return {
    opacity: transition
  };
};

const NavigationConfig = () => {
  return {
    screenInterpolator: sceneProps => {
      const position = sceneProps.position;
      const scene = sceneProps.scene;
      const index = scene.index;

      return FadeTransition(index, position);
    }
  };
};

export const MainNavigator = createStackNavigator(
  {
    SearchPage: { screen: SearchPage },
    SearchResultsPage: { screen: SearchResultsPage },
    FavoritesPage: { screen: FavoritesPage },
    MessagesPage: { screen: MessagesPage },
    MyProfilePage: { screen: MyProfilePage },
    HomeOwnerPage: { screen: HomeOwnerPage }
  },
  {
    transitionConfig: NavigationConfig,
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);
