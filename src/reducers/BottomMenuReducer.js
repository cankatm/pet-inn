import {
  SHOW_BOTTOM_MENU_BIG_BUTTON,
  SHOW_BOTTOM_MENU
} from '../helpers/ActionNames.js';

const INITIAL_STATE = {
  showBottomMenuBigButton: true,
  showBottomMenu: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_BOTTOM_MENU_BIG_BUTTON:
      return { ...state, showBottomMenuBigButton: action.payload };
    case SHOW_BOTTOM_MENU:
      return { ...state, showBottomMenu: action.payload };
    default:
      return state;
  }
};
