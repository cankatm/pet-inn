import { combineReducers } from 'redux';

import LanguageReducer from './LanguageReducer';
import BottomMenuReducer from './BottomMenuReducer';
import ImageReducer from './ImageReducer';

export default combineReducers({
    languageReducer: LanguageReducer,
    bottomMenuReducer: BottomMenuReducer,
    imageReducer: ImageReducer
});