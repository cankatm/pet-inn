import { 
    SELECT_LANGUAGE 
} from '../helpers/ActionNames.js';

const INITIAL_STATE = {
    language: 'turkish'
};
  
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SELECT_LANGUAGE:
            return { ...state, language: action.payload };
        default:
            return state;
    }
};