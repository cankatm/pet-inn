import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  Animated
} from 'react-native';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

import { HomeOwnerCard } from '../components/SearchResultItems';
import { BackHeader } from '../components/Headers';
import { CenteredArea } from '../components/CenteredAreas';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import { dummyUsers } from '../helpers/DummyUsers';

class SearchResultsPage extends Component {
  state = {
    scrollAnimation: new Animated.Value(0)
  };
  render() {
    const { scrollAnimation } = this.state;
    const sliderBackgroundInterpolate = scrollAnimation.interpolate({
      inputRange: [0, defaults.HEIGHT / 2, defaults.HEIGHT],
      outputRange: [colors.white, colors.orange, 'red'],
      extrapolate: 'clamp'
    });

    let translateY = scrollAnimation.interpolate({
      inputRange: [0, defaults.headerHeightAnimationValue],
      outputRange: [0, -defaults.headerHeightAnimationValue],
      extrapolate: 'clamp'
    });

    return (
      <View
        style={styles.SearchResultsPageStyles.searchResultsPageContainerStyle}
      >
        <AnimatedFlatList
          contentContainerStyle={{ marginTop: 100 }}
          scrollEventThrottle={16} // <-- Use 1 here to make sure no events are ever missed
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: { y: scrollAnimation }
                }
              }
            ],
            { useNativeDriver: true } // <-- Add this
          )}
          data={dummyUsers}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <HomeOwnerCard index={index} user={item} />
          )}
          keyExtractor={item => item.id.toString()}
          ListFooterComponent={<View style={{ height: 200 }} />}
        />

        {/* BackHeader must be at bottom for animation purposes */}
        <BackHeader translateY={translateY} scrollAnimation={scrollAnimation} />
      </View>
    );
  }
}

export default SearchResultsPage;
