import SearchPageStyles from './styles/SearchPageStyles';
import SearchResultsPageStyles from './styles/SearchResultsPageStyles';

export { SearchPageStyles, SearchResultsPageStyles };
