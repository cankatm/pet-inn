import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Animated, Image } from 'react-native';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { CenteredArea } from '../components/CenteredAreas';
import {
  showBottomMenuBigButtonAction,
  showBottomMenuAction
} from '../actions';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import { logoWritingImage } from '../helpers/Images';

import deerAnimation from '../../assets/animations/deer.json';
import hamburger from '../../assets/animations/hamburger.json';
import loading from '../../assets/animations/loading.json';
import loading1 from '../../assets/animations/loading (1).json';
import dog from '../../assets/animations/dog.json';

const INITIAL_TOP = 17;

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: dog,
      button1Pressed: false,
      firstAnimationFinished: false
    };

    this.buttonAnimation1 = new Animated.Value(0);
    this.iconAnimation1 = new Animated.Value(0);
  }

  // componentDidMount() {
  //   this.playLottieAnimation();
  // }

  // playLottieAnimation = () => {
  //   if (this.state.animation) {
  //     this.animation.play();
  //   }
  // };

  handleButton1Press = () => {
    const { showBottomMenuBigButtonAction } = this.props;
    showBottomMenuBigButtonAction(false);

    Animated.timing(this.buttonAnimation1, {
      toValue: 1,
      duration: 250
    }).start(({ finished }) => {
      if (finished) {
        this.setState({ firstAnimationFinished: true });
        this.handleJumpAnimation();
      }
    });
    this.setState({ button1Pressed: !this.state.button1Pressed });
  };

  handleJumpAnimation = () => {
    const { showBottomMenuBigButtonAction } = this.props;

    Animated.timing(this.iconAnimation1, {
      toValue: 1,
      duration: 500
    }).start(({ finished }) => {
      if (finished) {
        Animated.timing(this.iconAnimation1, {
          toValue: 0,
          duration: 500
        }).start(({ finished }) => {
          if (finished) {
            this.handleJumpAnimation();
            const { navigation, showBottomMenuAction } = this.props;

            navigation.navigate('SearchResultsPage');
            showBottomMenuAction(false);

            // TODO: burası önemli
            // this.buttonAnimation1.setValue(0);
            // this.iconAnimation1.setValue(0);
            // showBottomMenuBigButtonAction(true);
          }
        });
      }
    });
  };

  render() {
    const backgroundColorInterpolate = this.buttonAnimation1.interpolate({
      inputRange: [0, 1],
      outputRange: [colors.white, colors.orange],
      extrapolate: 'clamp'
    });

    const backgroundColorStyle = {
      backgroundColor: backgroundColorInterpolate
    };

    const buttonWidth1Interpolate = this.buttonAnimation1.interpolate({
      inputRange: [0, 1],
      outputRange: [2, 32],
      extrapolate: 'clamp'
    });

    const button1AnimationStyle = {
      borderWidth: buttonWidth1Interpolate
    };

    const elementOpacityInterpolate = this.buttonAnimation1.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });

    const elementOpacityStyle = {
      opacity: elementOpacityInterpolate
    };

    const iconTopPositionInterpolate = this.buttonAnimation1.interpolate({
      inputRange: [0, 1],
      outputRange: [
        (defaults.homePageButtonSize - defaults.homePageIconSize) / 2 + 1,
        INITIAL_TOP
      ],
      extrapolate: 'clamp'
    });

    const iconLeftPositionInterpolate = this.buttonAnimation1.interpolate({
      inputRange: [0, 1],
      outputRange: [
        (defaults.homePageButtonSize - defaults.homePageIconSize) / 2 + 5,
        23
      ],
      extrapolate: 'clamp'
    });

    const iconPositionStyle = {
      top: iconTopPositionInterpolate,
      left: iconLeftPositionInterpolate
    };

    const iconJumpPositionInterpolate = this.iconAnimation1.interpolate({
      inputRange: [0, 0.25, 0.5, 0.75, 1],
      outputRange: [
        INITIAL_TOP,
        INITIAL_TOP - 2,
        INITIAL_TOP,
        INITIAL_TOP - 6,
        INITIAL_TOP
      ],
      extrapolate: 'clamp'
    });

    const iconJumpStyle = {
      top: iconJumpPositionInterpolate
    };

    return (
      <Animated.View
        style={[
          styles.SearchPageStyles.searchPageContainerStyle,
          backgroundColorStyle
        ]}
      >
        <View style={{ position: 'absolute', top: 16, left: 0 }}>
          <CenteredArea>
            <Animated.Image
              source={logoWritingImage}
              style={[{ width: 200, height: 120 }, elementOpacityStyle]}
              resizeMode='contain'
            />
          </CenteredArea>
        </View>

        <View style={{ position: 'absolute', top: 120, left: 0 }}>
          <CenteredArea>
            <Animated.Text style={elementOpacityStyle}>
              Find near hotels for your pet
            </Animated.Text>
          </CenteredArea>
        </View>

        <View style={{ position: 'absolute', top: 180, left: 0 }}>
          <CenteredArea>
            <TouchableOpacity
              activeOpacity={1}
              onPress={this.handleButton1Press}
            >
              <Animated.View
                style={[
                  styles.SearchPageStyles.searchPageSearchButtonContainerStyle,
                  button1AnimationStyle
                ]}
              >
                <Animated.View
                  style={[
                    styles.SearchPageStyles.searchPageIconContainerStyle,
                    iconPositionStyle,
                    this.state.firstAnimationFinished ? iconJumpStyle : null
                  ]}
                >
                  <Icon
                    name='ios-pin'
                    size={defaults.homePageIconSize}
                    color={colors.orange}
                  />
                </Animated.View>
              </Animated.View>
            </TouchableOpacity>
          </CenteredArea>
        </View>

        {/* <View style={{ position: 'absolute', top: 360, left: 64 }}>
          <CenteredArea>
            {this.state.animation && (
              <Lottie
                speed={1}
                loop={true}
                ref={animation => {
                  this.animation = animation;
                }}
                style={{
                  width: 200,
                  height: 200
                }}
                source={this.state.animation}
              />
            )}
          </CenteredArea>
        </View> */}
      </Animated.View>
    );
  }
}

export default connect(
  null,
  { showBottomMenuBigButtonAction, showBottomMenuAction }
)(HomePage);
