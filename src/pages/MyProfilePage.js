import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';

import styles from './styles';

class MyProfilePage extends Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                <Text>MyProfilePage</Text>
            </View>
        );
    }
}

export default MyProfilePage;