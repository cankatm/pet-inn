import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';

import styles from './styles';

class MessagesPage extends Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                <Text>Messages page</Text>
            </View>
        );
    }
}

export default MessagesPage;