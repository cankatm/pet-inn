import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
  searchPageContainerStyle: {
    flex: 1,
    backgroundColor: colors.white,
    width: defaults.WIDTH,
    height: defaults.HEIGHT
  },
  searchPageButtonsContainerStyle: {
    flexDirection: 'row',
    width: defaults.WIDTH - 64,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  searchPageSearchButtonContainerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: defaults.homePageButtonSize,
    height: defaults.homePageButtonSize,
    borderRadius: defaults.homePageButtonSize / 2,
    backgroundColor: colors.white,
    borderColor: colors.orange
  },
  searchPageIconContainerStyle: {
    position: 'absolute',
    left: (defaults.homePageButtonSize - defaults.homePageIconSize) / 2 + 4,
    top: (defaults.homePageButtonSize - defaults.homePageIconSize) / 2
  }
});
