import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
  searchResultsPageContainerStyle: {
    flex: 1,
    backgroundColor: colors.white
  }
});
