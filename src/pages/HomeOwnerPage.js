import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import styles from './styles';

class HomeOwnerPage extends Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                <Text>Home owner page</Text>
            </View>
        );
    }
}

export default HomeOwnerPage;